function firstPriceActive() {
  var firstPrice = document.getElementById("firstPrice");
  var secondPrice = document.getElementById("secondPrice");
  firstPrice.classList.add("active");
  secondPrice.classList.remove("active");
}
function secondPriceActive() {
  var firstPrice = document.getElementById("firstPrice");
  var secondPrice = document.getElementById("secondPrice");
  secondPrice.classList.add("active");
  firstPrice.classList.remove("active");
}

function priceActive1() {
  let first = document.getElementById("first");
  let second = document.getElementById("second");
  let third = document.getElementById("third");
  first.classList.add("active");
  second.classList.remove("active");
  third.classList.remove("active");
}
function priceActive2() {
  let first = document.getElementById("first");
  let second = document.getElementById("second");
  let third = document.getElementById("third");
  first.classList.remove("active");
  second.classList.add("active");
  third.classList.remove("active");
}
function priceActive3() {
  let first = document.getElementById("first");
  let second = document.getElementById("second");
  let third = document.getElementById("third");
  first.classList.remove("active");
  second.classList.remove("active");
  third.classList.add("active");
}
function finalResult() {
  let finalResult = document.getElementById("finalResult");
  var firstPrice = document.getElementById("firstPrice");
  var secondPrice = document.getElementById("secondPrice");
  let first = document.getElementById("first");
  let second = document.getElementById("second");
  let third = document.getElementById("third");

  if (firstPrice.classList.contains("active")) {
    if (first.classList.contains("active")) {
      finalResult.innerHTML = "4";
    } else if (second.classList.contains("active")) {
      finalResult.innerHTML = "3";
    } else if (third.classList.contains("active")) {
      finalResult.innerHTML = "2";
    }
  } else if (secondPrice.classList.contains("active")) {
    if (first.classList.contains("active")) {
      finalResult.innerHTML = "5";
    } else if (second.classList.contains("active")) {
      finalResult.innerHTML = "4";
    } else if (third.classList.contains("active")) {
      finalResult.innerHTML = "3";
    }
  } else {
    finalResult.innerHTML = "3";
  }
}
