let modules = [
  {
    week: "1",
    description: "Introduction to Scrum and Agile ways of work",
  },
  {
    week: "2",
    description: "Manual Testing Fundamentals",
  },
  {
    week: "3-6",
    description: "Introduction to OOP ( Java )",
  },
  {
    week: "7",
    description: "Introduction to Automation Testing",
  },
  {
    week: "8-10",
    description: "Back-end Test Automation using rest assured ( Java )",
  },
  {
    week: "11-13",
    description: "Front-end Test Automation using Selenium (Java) - GUI",
  },
  {
    week: "14",
    description: "Database connection - Postgre",
  },
  {
    week: "15",
    description: "End 2 End Testing",
  },
  {
    week: "16",
    description: "Testing Best Practices",
  },
  {
    week: "17",
    description: "Prepare for Job Interview",
  },
];
function createElementsFromArray() {
  let array = modules.map(function (item) {
    return (
      "<div id='box' class='box' onclick='addActiveClass(this)'><p><span> Week " +
      item.week +
      ": </span>" +
      item.description +
      "</p></div>"
    );
  });
  document.write(array.join(""));
}
function addActiveClass(element) {
  removeActiveClass();
  element.classList.add("active");
}

function removeActiveClass() {
  let elements = document.querySelectorAll(".box");
  elements.forEach((box) => box.classList.remove("active"));
}

function addEventListenersToBoxes() {
  let boxes = document.querySelectorAll(".box");
  boxes.forEach((box) => {
    box.addEventListener("click", function () {
      addActiveClass(this);
      createDescriptionBox();
    });
  });
}
//

function createDescriptionBox() {
  let boxes = document.querySelectorAll(".box");
  let descriptionBox = document.querySelectorAll(".descriptionBox");

  for (const box of boxes) {
    const paragraph = box.querySelector("p");
    if (
      paragraph.textContent.includes("Week 1:") &&
      box.classList.contains("active")
    ) {
      descriptionBox = `
            <div class="descriptionBoxTitle">
              <p>Week 1: Introduction to Software Testing</p>
            </div>
            <div class="descriptionBoxDescription">
              <p class="animate__animated animate__zoomIn">
              Overview of software testing
              Importance of software testing
              Software development life cycle ( SDLC )
              Different types of testing ( functional, non-functional, etc. )
              Introduction to manual testing
              </p>
            </div>
        `;

      document.getElementById("descriptionBox").innerHTML = descriptionBox;
    } else if (
      paragraph.textContent.includes("Week 2:") &&
      box.classList.contains("active")
    ) {
      descriptionBox = `
            <div class="descriptionBoxTitle">
              <p>Week 2: Manual Testing Fundamentals</p>
            </div>
            <div class="descriptionBoxDescription">
              
            <p class="animate__animated animate__zoomIn">
              Acceptance criteria
              Test case design techniques
              Test planning and documentation
              Test execution and defect/bug reporting
              Test management tools
              </p>
              <p class="animate__animated animate__zoomIn"><span>Hands-on practice: Manual test case creation and execution</span></p>
            </div>
        `;

      document.getElementById("descriptionBox").innerHTML = descriptionBox;
    } else if (
      paragraph.textContent.includes("Week 3-6: ") &&
      box.classList.contains("active")
    ) {
      descriptionBox = `
            <div class="descriptionBoxTitle">
              <p>Week 3-6: Introduction to OOP ( Java )</p>
            </div>
            <div class="descriptionBoxDescription">
              
            <p class="animate__animated animate__zoomIn">
             <strong>Key concepts: classes, objects, inheritance, polymorphism, encapsulation</strong> 
            <br><br>
            Introduction to the Java programming language (or any other OOP language of choice)
            Defining classes and objects
            Class properties (fields) and methods
            Constructors and object initialization
            Access modifiers (public, private, protected)

            <br><br>

            <span>Hands-on practice: Creating and working with classes and objects</span>
            <br><br>

            Inheritance hierarchy and relationships
            Extending classes and creating subclasses
            Method overriding and polymorphic behavior
            Abstract classes and interfaces

            <br><br>
            <span>Hands-on practice: Implementing inheritance and polymorphism</span>
            <br><br>
            Encapsulation principles
            Getters and setters for data access
            Accessor and mutator methods
            Abstraction and interface design
            <br><br>
            
            <span>Hands-on practice: Applying encapsulation and abstraction concepts</span>
              </p>
            </div>
        `;

      document.getElementById("descriptionBox").innerHTML = descriptionBox;
    } else if (
      paragraph.textContent.includes("Week 7: ") &&
      box.classList.contains("active")
    ) {
      descriptionBox = `
        <div class="descriptionBoxTitle">
          <p>Week 7: Introduction to Automation Testing</p>
        </div>
        <div class="descriptionBoxDescription">
          
        <p class="animate__animated animate__zoomIn">
          Introduction to automation testing
          Benefits and challenges of automation testing
          Test automation frameworks (structure)
          Popular automation tools (e.g., Selenium, Appium)
          Back end (API) testing vs Front end testing
          Unit/testing</p>
        </div>
    `;

      document.getElementById("descriptionBox").innerHTML = descriptionBox;
    } else if (
      paragraph.textContent.includes("Week 8-10: ") &&
      box.classList.contains("active")
    ) {
      descriptionBox = `
        <div class="descriptionBoxTitle">
          <p>Week 8-10: Back-end Test Automation using 
          rest assured ( Java ) - test classes, models, client etc.</p>
        </div>
        <div class="descriptionBoxDescription">
          
        <p class="animate__animated animate__zoomIn">
          What is rest API ? <br> What is rest assured ? <br> How to connect and test a service (API) ? <br> What is API? <br> Test classes Models Structure of a framework Creating an automated test
          </p>
          <p class="animate__animated animate__zoomIn"><span> Hands-on practice: Create framework with automated tests (back end)</span></p>
        </div>
    `;

      document.getElementById("descriptionBox").innerHTML = descriptionBox;
    } else if (
      paragraph.textContent.includes("Week 11-13: ") &&
      box.classList.contains("active")
    ) {
      descriptionBox = `
        <div class="descriptionBoxTitle">
        <p>Week 11-13: Front-end Test Automation using 
          Selenium ( Java ) - GUI</p>
        </div>
        <div class="descriptionBoxDescription">
          
        <p class="animate__animated animate__zoomIn">
          What is GUI (graphical user interface) ? <br> What is selenium ? <br> Selectors <br> POM (page object model) structure
          </p>
          <p class="animate__animated animate__zoomIn"><span> Hands-on practice: Create framework with automated tests (front end)</span></p>
        </div>
    `;

      document.getElementById("descriptionBox").innerHTML = descriptionBox;
    } else if (
      paragraph.textContent.includes("Week 14:") &&
      box.classList.contains("active")
    ) {
      descriptionBox = `
        <div class="descriptionBoxTitle">
          <p>Week 14: Database connection - Postgre</p>
        </div>
        <div class="descriptionBoxDescription">
          
        <p class="animate__animated animate__zoomIn">
          Types of database <br>
            How to connect to a database ? <br>
            How to view database ? <br>
            How to connect a database with a automation testing
            framework ? <br>
            Introduction in Postgre database
          </p>
        </div>
    `;

      document.getElementById("descriptionBox").innerHTML = descriptionBox;
    } else if (
      paragraph.textContent.includes("Week 15:") &&
      box.classList.contains("active")
    ) {
      descriptionBox = `
        <div class="descriptionBoxTitle">
          <p>Week 15: End 2 End Testing</p>
        </div>
        <div class="descriptionBoxDescription">
          
        <p class="animate__animated animate__zoomIn">
          Testing a system as a whole ( back-end + front-end + database )
          </p>
        </div>
    `;

      document.getElementById("descriptionBox").innerHTML = descriptionBox;
    } else if (
      paragraph.textContent.includes("Week 16:") &&
      box.classList.contains("active")
    ) {
      descriptionBox = `
        <div class="descriptionBoxTitle">
          <p>Week 16: Testing Best Practices</p>
        </div>
        <div class="descriptionBoxDescription">
          
        <p class="animate__animated animate__zoomIn">
          Code refactoring <br>
        Flaky tests <br>
        Testing reports <br>
        Other practices <br>

          </p>
        </div>
    `;

      document.getElementById("descriptionBox").innerHTML = descriptionBox;
    } else if (
      paragraph.textContent.includes("Week 17:") &&
      box.classList.contains("active")
    ) {
      descriptionBox = `
        <div class="descriptionBoxTitle">
          <p>Week 17: Prepare for Job Interview</p>
        </div>
        <div class="descriptionBoxDescription">
          
        <p class="animate__animated animate__zoomIn">
          Interview questions for QA Tester <br>
            Project ideas   <br>
            How to land your first job <br> 
          </p>
        </div>
    `;

      document.getElementById("descriptionBox").innerHTML = descriptionBox;
    }
  }
}
